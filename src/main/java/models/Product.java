package models;

import common.enums.Category;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Objects;

import static common.utilities.CommonConstants.*;

@Builder
@Getter
public class Product {

    private String name;
    private BigDecimal priceHT;
    private boolean isImported;
    private Category category;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name);

        if(isImported)
            stringBuilder.append(IMPORTED);

        return stringBuilder.append(A)
                .append(priceHT)
                .append(CURRENCY).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return isImported == product.isImported &&
                Objects.equals(name, product.name) &&
                Objects.equals(priceHT, product.priceHT) &&
                category == product.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, priceHT, isImported, category);
    }
}

