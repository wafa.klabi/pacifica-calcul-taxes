package models;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Objects;

import static common.utilities.CommonConstants.*;

@Builder
@Getter
public class Order {

    private Product product;
    private int quantity;
    private BigDecimal orderAmount;
    private BigDecimal taxAmount;

    @Override
    public String toString() {
        return STAR +
                quantity +
                SPACE +
                product.toString() +
                TWO_POINTS +
                orderAmount +
                CURRENCY +
                SPACE +
                TTC +
                NEW_LINE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return quantity == order.quantity &&
                Objects.equals(product, order.product) &&
                Objects.equals(orderAmount, order.orderAmount) &&
                Objects.equals(taxAmount, order.taxAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity, orderAmount, taxAmount);
    }
}
