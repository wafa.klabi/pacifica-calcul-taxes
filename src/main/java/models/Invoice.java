package models;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static common.utilities.CommonConstants.*;

@Builder
@Getter
public class Invoice {

    private List<Order> orderList;
    private BigDecimal totalAmount;
    private BigDecimal totalTaxAmount;

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        orderList.forEach( order -> result.append(order.toString()));

        return result.append(NEW_LINE)
                .append(TOTAL_TAXES)
                .append(TWO_POINTS)
                .append(totalTaxAmount)
                .append(CURRENCY)
                .append(NEW_LINE)
                .append(TOTAL)
                .append(TWO_POINTS)
                .append(totalAmount)
                .append(CURRENCY).toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Invoice)) return false;
        Invoice invoice = (Invoice) o;
        return Objects.equals(orderList, invoice.orderList) &&
                Objects.equals(totalAmount, invoice.totalAmount) &&
                Objects.equals(totalTaxAmount, invoice.totalTaxAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderList, totalAmount, totalTaxAmount);
    }
}
