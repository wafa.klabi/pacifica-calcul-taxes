package common.utilities;

import common.enums.Category;
import models.Order;
import models.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CalculationUtility {

    private CalculationUtility() {
    }

    public static BigDecimal roundNearestFiveCent(BigDecimal value){
        return (value.divide(BigDecimal.valueOf(0.05),0, RoundingMode.UP))
                .multiply(BigDecimal.valueOf(0.05));
    }


    public static BigDecimal getAppliedTaxRateByProduct(Product product){
        return getTaxRate(product.getCategory(), product.isImported())
                .multiply(product.getPriceHT());
    }

    private  static BigDecimal getTaxRate(Category category, boolean isImported){
        return BigDecimal.ZERO
                .add(category.getTaxRate())
                .add( isImported ? BigDecimal.valueOf(0.05) : BigDecimal.ZERO);
    }

    public static BigDecimal multiplyQuantityAndAmount(int quantity, BigDecimal amount){
        return  amount.multiply(BigDecimal.valueOf(quantity));
    }

    public static BigDecimal calculateTaxAmountByOrder(Order order){
        return order.getOrderAmount().subtract(order.getProduct().getPriceHT()
                .multiply(BigDecimal.valueOf(order.getQuantity())));
    }
}
