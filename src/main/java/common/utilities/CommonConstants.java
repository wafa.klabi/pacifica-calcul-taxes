package common.utilities;

public final class CommonConstants {

    private CommonConstants() {
    }

    public static final String SPACE = " ";
    public static final String STAR = "* ";
    public static final String TWO_POINTS = " : ";
    public static final String TOTAL_TAXES = "Montant des taxes";
    public static final String TOTAL = "Total";
    public static final String TTC = "TTC";
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String IMPORTED = " importé";
    public static final String IMPORT = " import";
    public static final String A = " à ";
    public static final String CURRENCY = "€";
    public static final String EMPTY_STRING = "";
    public static final String REGEX_INT = "\\D+";
    public static final String BASKET_PRODUCT_NUMBER = "Donnez le nombre des produits dans le panier: " ;
    public static final String INPUT = "### INPUT" ;
    public static final String OUTPUT = "### OUTPUT" ;

    //Exception
    public static final String PRODUCT_INPUT_CANT_BLANK = "Product input can not be blank";

    //Library
    public final static String BOOKS = "livres";
    public final static String CHOCOLATE_BAR = "barres de chocolat";
    public final static String CHOCOLATE_BOX = "boîtes de chocolats";
    public final static String MEDICINES_BOXES = "boîtes de pilules contre la migraine";
}
