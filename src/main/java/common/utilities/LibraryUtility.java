package common.utilities;

import common.enums.Category;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static common.utilities.CommonConstants.*;

public final class LibraryUtility {

    private LibraryUtility() {
    }

    public static Map<Category, Set<String>> getLibrary(){
        Map<Category, Set<String>> categoryMap = new HashMap<>();
        categoryMap.put(Category.BOOKS, new HashSet<>() {{
            add(BOOKS);
        }});

        categoryMap.put(Category.ESSENTIALS, new HashSet<>() {{
            add(CHOCOLATE_BAR);
            add(CHOCOLATE_BOX);
            add(MEDICINES_BOXES);
        }});

        return categoryMap;
    }
}
