package common.utilities;

import common.enums.Category;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Scanner;

import static common.utilities.LibraryUtility.getLibrary;

public final class ScanUtility {

    private ScanUtility() {
    }

    public static int getQuantityFromInput(String productInput){
        return new Scanner(productInput)
                .useDelimiter(CommonConstants.REGEX_INT)
                .nextInt();
    }

    public static String getProductNameFromInput(String productInput){

        return productInput.contains(CommonConstants.IMPORT) ? productInput.substring(0,productInput.indexOf(CommonConstants.IMPORT))
                : productInput.substring(0,productInput.indexOf(CommonConstants.A));
    }

    public static BigDecimal getPriceHTFromInput(String productInput){
        return new BigDecimal(productInput
                .substring(productInput.indexOf(CommonConstants.A)+3));
    }

    public static Category getCategoryFromProductName(String productName) {

        return getLibrary().entrySet()
                .stream()
                .filter(library -> library.getValue().contains(productName))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(Category.OTHERS);
    }
}
