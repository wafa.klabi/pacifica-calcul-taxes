package common.enums;

import java.math.BigDecimal;

public enum Category {

    ESSENTIALS(BigDecimal.ZERO),
    BOOKS(BigDecimal.valueOf(0.10)),
    OTHERS(BigDecimal.valueOf(0.20));

    private BigDecimal taxRate;

    Category(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }
}
