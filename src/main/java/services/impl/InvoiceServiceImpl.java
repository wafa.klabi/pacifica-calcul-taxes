package services.impl;

import common.utilities.CalculationUtility;
import models.Invoice;
import models.Order;
import services.InvoiceService;

import java.math.BigDecimal;
import java.util.List;

public class InvoiceServiceImpl implements InvoiceService {

    @Override
    public Invoice generateInvoiceFromOrderList(List<Order> orderList){

        BigDecimal totalAmount = new BigDecimal(orderList.stream()
                .map(Order::getOrderAmount)
                .reduce(BigDecimal.ZERO,BigDecimal::add)
                .stripTrailingZeros()
                .toPlainString());

        BigDecimal totalTaxAmount = new BigDecimal(orderList.stream()
                .map(CalculationUtility::calculateTaxAmountByOrder)
                .reduce(BigDecimal.ZERO,BigDecimal::add)
                .stripTrailingZeros()
                .toPlainString());

        return Invoice.builder()
                .orderList(orderList)
                .totalAmount(totalAmount)
                .totalTaxAmount(totalTaxAmount)
                .build();
    }
}
