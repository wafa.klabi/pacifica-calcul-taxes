package services.impl;

import models.Order;
import models.Product;
import services.OrderService;
import services.ProductService;
import services.TaxService;

import java.math.BigDecimal;

import static common.utilities.CalculationUtility.multiplyQuantityAndAmount;
import static common.utilities.CalculationUtility.roundNearestFiveCent;
import static common.utilities.ScanUtility.getQuantityFromInput;

public class OrderServiceImpl implements OrderService {

    private final TaxService taxService;
    private final ProductService productService;

    public OrderServiceImpl(TaxService taxService, ProductService productService) {
        this.taxService = taxService;
        this.productService = productService;
    }

    @Override
    public Order generateOrderFromInput(String inputProduct) {

        Product product = productService.generateProductFromInput(inputProduct);
        int quantity = getQuantityFromInput(inputProduct);

        return generateOrder(product, quantity);
    }

    private Order generateOrder(Product product, int quantity) {

        BigDecimal taxAmount =taxService.getAppliedTaxRateForOrder(product,quantity);
        BigDecimal orderAmount = new BigDecimal(roundNearestFiveCent(multiplyQuantityAndAmount(quantity,product.getPriceHT())
                .add(taxAmount))
                .stripTrailingZeros()
                .toPlainString());

        return Order.builder()
                .product(product)
                .quantity(quantity)
                .orderAmount(orderAmount)
                .taxAmount(taxAmount)
                .build();
    }
}
