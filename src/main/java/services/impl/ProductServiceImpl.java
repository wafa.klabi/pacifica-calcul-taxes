package services.impl;

import models.Product;
import services.ProductService;

import static common.utilities.CommonConstants.*;
import static common.utilities.ScanUtility.*;

public class ProductServiceImpl implements ProductService {

    @Override
    public Product generateProductFromInput(String productInput) {
        if(productInput.isBlank())
            throw new StringIndexOutOfBoundsException(PRODUCT_INPUT_CANT_BLANK);

        productInput = productInput.substring(1)
                .replaceFirst(CURRENCY,EMPTY_STRING);

        String name = getProductNameFromInput(productInput
                .replaceFirst(String.valueOf( getQuantityFromInput(productInput)),EMPTY_STRING).trim());

        return Product.builder()
                .name(name)
                .isImported(productInput.contains(IMPORT))
                .priceHT(getPriceHTFromInput(productInput))
                .category(getCategoryFromProductName(name))
                .build();
    }
}
