package services.impl;

import models.Product;
import services.TaxService;

import java.math.BigDecimal;

import static common.utilities.CalculationUtility.*;

public class TaxServiceImpl implements TaxService {

    @Override
    public BigDecimal getAppliedTaxRateForOrder(Product product, int quantity) {
        return roundNearestFiveCent(getAppliedTaxRateByProduct(product).multiply(BigDecimal.valueOf(quantity)));
    }
}
