package services;

import models.Order;

public interface OrderService {
    Order generateOrderFromInput(String inputProduct);
}
