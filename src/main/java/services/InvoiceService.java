package services;

import models.Invoice;
import models.Order;

import java.util.List;

public interface InvoiceService {

    Invoice generateInvoiceFromOrderList(List<Order> orderList);
}
