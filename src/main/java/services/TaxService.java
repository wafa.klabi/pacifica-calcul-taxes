package services;

import models.Product;

import java.math.BigDecimal;

public interface TaxService {
    BigDecimal getAppliedTaxRateForOrder(Product product, int quantity);
}
