package services;

import models.Product;

public interface ProductService {
    Product generateProductFromInput(String inputEssentialsProduct);
}
