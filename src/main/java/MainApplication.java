import lombok.extern.slf4j.Slf4j;
import models.Order;
import org.apache.log4j.BasicConfigurator;
import services.InvoiceService;
import services.OrderService;
import services.impl.InvoiceServiceImpl;
import services.impl.OrderServiceImpl;
import services.impl.ProductServiceImpl;
import services.impl.TaxServiceImpl;

import java.util.*;
import java.util.stream.Collectors;

import static common.utilities.CommonConstants.*;

@Slf4j
public class MainApplication {

    private static InvoiceService invoiceService = new InvoiceServiceImpl();
    private static OrderService orderService = new OrderServiceImpl(new TaxServiceImpl(),new ProductServiceImpl());

    public static void main(String[] args) {

        BasicConfigurator.configure();

        Scanner entree = new Scanner(System.in);
        System.out.println(BASKET_PRODUCT_NUMBER);
        int productsNumber = entree.nextInt();

        System.out.println(NEW_LINE + INPUT);
        List<String> inputs = new ArrayList<>();

        int index = 0;

        while (index <= productsNumber) {
            String input = entree.nextLine();
            if (!input.isBlank())
                inputs.add(input);
            index++;
        }

        List<Order> orderList = inputs.stream()
                .map(in -> orderService.generateOrderFromInput(in))
                .collect(Collectors.toList());

        String invoiceOutput = invoiceService.generateInvoiceFromOrderList(orderList)
                .toString();

        System.out.println(NEW_LINE + OUTPUT);
        System.out.println(invoiceOutput);
    }
}
