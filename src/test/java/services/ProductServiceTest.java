package services;

import common.enums.Category;
import models.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import services.impl.ProductServiceImpl;

import static org.junit.Assert.assertEquals;
import static testUtilities.TestConstants.*;

public class ProductServiceTest {

    @InjectMocks
    ProductService productService;

    @Before
    public void setUp(){
        productService = new ProductServiceImpl();
    }

    @Test
    public void generateBookProductAndQuantityFromInput(){
        Product product = productService.generateProductFromInput(INPUT_BOOK_PRODUCT);
        assertEquals(Category.BOOKS, product.getCategory());
    }

    @Test
    public void generateOthersProductAndQuantityFromInput(){
        Product product = productService.generateProductFromInput(INPUT_OTHERS_PRODUCT);
        assertEquals(Category.OTHERS, product.getCategory());
    }

    @Test
    public void generateEssentialsProductAndQuantityFromInput(){
        Product product = productService.generateProductFromInput(INPUT_ESSENTIALS_PRODUCT);
        assertEquals(Category.ESSENTIALS, product.getCategory());
    }

    @Test()
    public void generateProductFromEmptyInput(){

        try {
            productService.generateProductFromInput(EMPTY_STRING);
        } catch (StringIndexOutOfBoundsException e) {
            assertEquals(PRODUCT_INPUT_CANT_BLANK,e.getMessage());
        }
    }
}
