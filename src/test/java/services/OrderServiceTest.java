package services;

import models.Order;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import services.impl.OrderServiceImpl;
import services.impl.ProductServiceImpl;
import services.impl.TaxServiceImpl;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static testUtilities.TestConstants.*;

public class OrderServiceTest {

    @InjectMocks
    OrderService orderService;

    @Before
    public void setUp(){
        orderService = new OrderServiceImpl(new TaxServiceImpl(),new ProductServiceImpl());
    }

    @Test
    public void generateOrderFromImportedChocolateInput(){

        Order order = orderService.generateOrderFromInput(INPUT_IMPORTED_CHOCOLATE_PRODUCT);
        BigDecimal expected =new BigDecimal(VALUE_TWENTY_ONE);
        boolean isImported = order.getProduct().isImported();
        assertEquals(expected, order.getOrderAmount());
        assertTrue(isImported);
    }

    @Test
    public void generateOrderFromMedicinesInput(){
        Order order = orderService.generateOrderFromInput(INPUT_MEDICINES_PRODUCT);
        BigDecimal expected =new BigDecimal(VALUE_TWENTY_NINE_POINT_TWENTY_FIVE);
        boolean isImported = order.getProduct().isImported();
        assertEquals(expected, order.getOrderAmount());
        assertFalse(isImported);
    }

    @Test
    public void generateOrderFromImportedPerfumesInput(){
        Order order = orderService.generateOrderFromInput(INPUT_THREE_IMPORTED_PERFUMES_PRODUCT);
        BigDecimal expected =new BigDecimal(VALUE_ONE_HUNDRED_SEVENTY_EIGHT_POINT_FIFTEEN);
        boolean isImported = order.getProduct().isImported();
        assertEquals(expected, order.getOrderAmount());
        assertTrue(isImported);
    }

    @Test
    public void generateOrderFromBookInput(){

        Order order = orderService.generateOrderFromInput(INPUT_BOOK_PRODUCT);
        BigDecimal expected =new BigDecimal(VALUE_TWENTY_SEVEN_POINT_FIVE);
        boolean isImported = order.getProduct().isImported();
        assertEquals(expected, order.getOrderAmount());
        assertFalse(isImported);
    }
}
