package services;

import common.enums.Category;
import models.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import services.impl.TaxServiceImpl;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static testUtilities.TestConstants.*;

public class TaxServiceTest {

    @InjectMocks
    TaxService taxService;

    @Before
    public void setUp(){
        taxService = new TaxServiceImpl();
    }

    @Test
    public void calculateTaxAmountUsingEssentialsProduct(){
        Product essential = Product.builder()
                .name(CHOCOLATE)
                .priceHT(BigDecimal.valueOf(0.85))
                .isImported(Boolean.FALSE)
                .category(Category.ESSENTIALS)
                .build();

        BigDecimal tax = taxService.getAppliedTaxRateForOrder(essential,3);
        BigDecimal expected = new BigDecimal(VALUE_ZERO);
        assertEquals(expected, tax);
    }

    @Test
    public void calculateTaxAmountUsingBooksProduct(){
        Product book = Product.builder()
                .name(BOOK)
                .priceHT(BigDecimal.valueOf(12.49))
                .isImported(Boolean.FALSE)
                .category(Category.BOOKS)
                .build();

        BigDecimal tax = taxService.getAppliedTaxRateForOrder(book,2);
        BigDecimal expected = new BigDecimal(VALUE_TWO_POINT_FIVE);
        assertEquals(expected, tax);
    }

    @Test
    public void calculateTaxAmountUsingOthersProduct(){
        Product other = Product.builder()
                .name(MUSICAL_CD)
                .priceHT(BigDecimal.valueOf(14.99))
                .isImported(Boolean.FALSE)
                .category(Category.OTHERS)
                .build();

        BigDecimal tax = taxService.getAppliedTaxRateForOrder(other,1);
        BigDecimal expected = new BigDecimal(VALUE_THREE);
        assertEquals(expected, tax);
    }
}
