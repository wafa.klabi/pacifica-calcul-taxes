package services;

import models.Invoice;
import models.Order;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import services.impl.InvoiceServiceImpl;

import java.io.IOException;
import java.util.List;

import static testUtilities.FileUtility.getDataFromFile;
import static testUtilities.InputsUtility.*;
import static testUtilities.TestConstants.*;

public class InvoiceServiceTest {

    @InjectMocks
    InvoiceService invoiceService;

    @Before
    public void setUp(){
        invoiceService = new InvoiceServiceImpl();
    }

    @Test
    public void generateFirstInvoiceOutputTest() throws IOException {

        String expectedData = getDataFromFile(TEST_FIRST_FILE).trim();

        List<Order> orderList = createFirstInput();

        Invoice invoice = invoiceService.generateInvoiceFromOrderList(orderList);
        String data = invoice.toString().stripTrailing();
        Assert.assertEquals(expectedData, data);
    }


    @Test
    public void generateSecondInvoiceOutputTest() throws IOException {

        String expectedData = getDataFromFile(TEST_SECOND_FILE).trim();

        List<Order> orderList = createSecondInput();

        Invoice invoice = invoiceService.generateInvoiceFromOrderList(orderList);
        String data = invoice.toString().stripTrailing();
        Assert.assertEquals(expectedData, data);
    }

    @Test
    public void generateThirdInvoiceOutputTest() throws IOException {

        String expectedData = getDataFromFile(TEST_THIRD_FILE).trim();

        List<Order> orderList = createThirdInput();

        Invoice invoice = invoiceService.generateInvoiceFromOrderList(orderList);
        String data = invoice.toString().stripTrailing();
        Assert.assertEquals(expectedData, data);
    }
}
