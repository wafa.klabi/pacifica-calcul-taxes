package testUtilities;

public final class TestConstants {

    private TestConstants() {
    }

    //inputs
    public static final String INPUT_BOOK_PRODUCT = "* 2 livres à 12.49€";
    public static final String INPUT_OTHERS_PRODUCT = "* 1 CD musical à 14.99€";
    public static final String INPUT_ESSENTIALS_PRODUCT = "* 3 barres de chocolat à 0.85€";
    public static final String INPUT_IMPORTED_CHOCOLATE_PRODUCT = "* 2 boîtes de chocolats importée à 10€";
    public static final String INPUT_THREE_IMPORTED_PERFUMES_PRODUCT = "* 3 flacons de parfum importé à 47.50€";
    public static final String INPUT_MEDICINES_PRODUCT = "* 3 boîtes de pilules contre la migraine à 9.75€";
    public static final String INPUT_IMPORTED_PERFUMES_PRODUCT = "* 2 flacons de parfum importé à 27.99€";
    public static final String INPUT_PERFUME_PRODUCT = "* 1 flacon de parfum à 18.99€";
    public static final String INPUT_IMPORTED_CHOCOLATE_BOX_PRODUCT = "* 2 boîtes de chocolats importés  à 11.25€";
    public static final String EMPTY_STRING = "" ;
    public static final String CHOCOLATE = "barres de chocolat" ;
    public static final String BOOK = "livres" ;
    public static final String MUSICAL_CD = "CD musical" ;

    //values
    public static final String VALUE_ZERO = "0.00";
    public static final String VALUE_THREE = "3.00";
    public static final String VALUE_TWO_POINT_FIVE = "2.50";
    public static final String VALUE_TWENTY_ONE = "21";
    public static final String VALUE_TWENTY_SEVEN_POINT_FIVE = "27.5";
    public static final String VALUE_TWENTY_NINE_POINT_TWENTY_FIVE = "29.25";
    public static final String VALUE_ONE_HUNDRED_SEVENTY_EIGHT_POINT_FIFTEEN = "178.15";

    //Exception
    public static final String PRODUCT_INPUT_CANT_BLANK = "Product input can not be blank";

    //Output Files
    public static final String TEST_FIRST_FILE = "firstInvoiceOutputFileTest.txt";
    public static final String TEST_SECOND_FILE = "secondInvoiceOutputFileTest.txt";
    public static final String TEST_THIRD_FILE = "thirdInvoiceOutputFileTest.txt";
    public static final String UTF_8 = "UTF-8";
}
