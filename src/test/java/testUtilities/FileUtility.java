package testUtilities;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@UtilityClass
public class FileUtility {


    ClassLoader classLoader = FileUtility.class.getClassLoader();

    public static String getDataFromFile(String fileName) throws IOException {

        File file = new File(Objects.requireNonNull(classLoader.getResource(fileName)).getFile());
        return FileUtils.readFileToString(file, TestConstants.UTF_8);
    }
}
