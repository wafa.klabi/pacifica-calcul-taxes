package testUtilities;

import lombok.experimental.UtilityClass;
import models.Order;
import services.OrderService;
import services.impl.OrderServiceImpl;
import services.impl.ProductServiceImpl;
import services.impl.TaxServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static testUtilities.TestConstants.*;

public final class InputsUtility {

    private InputsUtility() {
    }

    public static List<Order> createFirstInput(){
        OrderService orderService = new OrderServiceImpl(new TaxServiceImpl(),new ProductServiceImpl());

        List<Order> firstInput = new ArrayList<>();
        Order firstOrder = orderService.generateOrderFromInput(INPUT_BOOK_PRODUCT);
        Order secondOrder = orderService.generateOrderFromInput(INPUT_OTHERS_PRODUCT);
        Order thirdOrder = orderService.generateOrderFromInput(INPUT_ESSENTIALS_PRODUCT);

        firstInput.add(firstOrder);
        firstInput.add(secondOrder);
        firstInput.add(thirdOrder);

        return firstInput;
    }

    public static List<Order> createSecondInput(){
        OrderService orderService = new OrderServiceImpl(new TaxServiceImpl(),new ProductServiceImpl());

        List<Order> secondInput = new ArrayList<>();

        Order firstOrder = orderService.generateOrderFromInput(INPUT_IMPORTED_CHOCOLATE_PRODUCT);
        Order secondOrder = orderService.generateOrderFromInput(INPUT_THREE_IMPORTED_PERFUMES_PRODUCT);

        secondInput.add(firstOrder);
        secondInput.add(secondOrder);

        return secondInput;
    }

    public static List<Order> createThirdInput(){
        OrderService orderService = new OrderServiceImpl(new TaxServiceImpl(),new ProductServiceImpl());

        List<Order> thirdInput = new ArrayList<>();

        Order firstOrder = orderService.generateOrderFromInput(INPUT_IMPORTED_PERFUMES_PRODUCT);
        Order secondOrder = orderService.generateOrderFromInput(INPUT_PERFUME_PRODUCT);
        Order thirdOrder = orderService.generateOrderFromInput(INPUT_MEDICINES_PRODUCT);
        Order fourthOrder = orderService.generateOrderFromInput(INPUT_IMPORTED_CHOCOLATE_BOX_PRODUCT);

        thirdInput.add(firstOrder);
        thirdInput.add(secondOrder);
        thirdInput.add(thirdOrder);
        thirdInput.add(fourthOrder);

        return thirdInput;
    }
}
